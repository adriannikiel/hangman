﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hangman
{
    class Program
    {
        static string[] WORDS = { "programming", "testing", "development", "realise", "management" };
        static Random random = new Random();

        static void Main(string[] args)
        {
            string searchingWord = PickAWord();
            HashSet<string> goodLetters = new HashSet<string>();

            int attempts = 6;

            while (true)
            {
                Console.Write("Write a letter: ");
                string letter = Console.ReadLine();

                if (searchingWord.Contains(letter))
                {
                    goodLetters.Add(letter);
                }
                else
                {
                    attempts--;
                }

                if (DrawGallons(attempts))
                {
                    Console.WriteLine("Correct word: " + searchingWord + "\n");
                    Console.WriteLine("You lose!");
                    break;
                }

                if (DrawPartialWord(searchingWord, goodLetters))
                {
                    Console.WriteLine("You win!");
                    break;
                }

            }
        }

        private static bool DrawPartialWord(string searchingWord, HashSet<string> usedLetters)
        {
            bool isFinished = true;

            StringBuilder builder = new StringBuilder(searchingWord);

            for (int i = 0; i < searchingWord.Length; i++)
            {
                string letter = Char.ToString(builder[i]);

                if (!usedLetters.Contains(letter))
                {
                    builder[i] = '_';
                    isFinished = false;
                }
            }

            Console.WriteLine(builder.ToString() + "\n");

            return isFinished;
        }

        private static string PickAWord()
        {
            int randomNumber = random.Next(WORDS.Length);
            return WORDS[randomNumber];
        }

        private static bool DrawGallons(int attempts)
        {
            bool isFinished = false;

            switch (attempts)
            {
                case 6:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 5:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 4:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |    |");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 3:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |   /|");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 2:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |   /|\\");
                    Console.WriteLine(" |");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 1:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |   /|\\");
                    Console.WriteLine(" |   /");
                    Console.WriteLine(" |");
                    Console.WriteLine("");
                    break;
                case 0:
                    Console.WriteLine(" ");
                    Console.WriteLine(" ------");
                    Console.WriteLine(" |/   |");
                    Console.WriteLine(" |    O");
                    Console.WriteLine(" |   /|\\");
                    Console.WriteLine(" |   / \\");
                    Console.WriteLine(" |");
                    Console.WriteLine("");

                    isFinished = true;
                    break;
            }

            return isFinished;
        }
    }
}